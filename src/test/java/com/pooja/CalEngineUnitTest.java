package com.pooja;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class CalEngineUnitTest {

	CalEngine calEngine = new CalEngine();

	@Test
	public void testAdd() {

		int val = calEngine.add(1, 2);

		assertEquals(val, 3);

	}

	@Test
	public void testSub() {
		int val = calEngine.sub(10, 3);

		assertEquals(val, 7);

	}

	@Test
	public void testMul() {

		int val = calEngine.mul(1, 5);

		assertEquals(val, 5);

	}

	@Test
	public void testDiv() {

		int val = calEngine.div(15, 5);

		assertEquals(val, 3);

	}

	@Test
	public void testPerformActionHappyScenarioAdd() {

		int val = calEngine.performAction("A", 100, 200);
		assertEquals(val, 300);

	}
	
	@Test
	public void testPerformActionHappyScenarioSub() {

		int val = calEngine.performAction("S", 200, 200);
		assertEquals(val, 0);

	}
	
	@Test
	public void testPerformActionHappyScenarioMul() {

		int val = calEngine.performAction("M", 1, 200);
		assertEquals(val, 200);

	}
	
	@Test
	public void testPerformActionHappyScenarioDiv() {

		int val = calEngine.performAction("D", 100, 2);
		assertEquals(val, 50);

	}
	
	@Test
	public void testPerformActionNegativeScenario() {

		int val = calEngine.performAction("Z", 100, 200);
		assertNotEquals(val, 300);

	}

}
