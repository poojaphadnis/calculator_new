package com.pooja;

public class CalEngine {

	public int add(int a, int b) {
		int result = a + b;

		return result;

	}

	public int sub(int a, int b) {
		int result = a - b;

		return result;

	}

	public int mul(int a, int b) {
		int result = a * b;

		return result;

	}

	public int div(int a, int b) {
		int result = a / b;

		return result;

	}

	public int performAction(String action, int i1, int i2) {

		int rel = 0;
		switch (action) {
		case "A":
			rel = add(i1, i2);

			break;

		case "S":
			rel = sub(i1, i2);
			break;

		case "M":
			rel = mul(i1, i2);
			break;

		case "D":
			rel = div(i1, i2);
			break;

		default:
			break;
		}
		
		return rel;
	}
}
